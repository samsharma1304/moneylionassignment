package com.moneylion.evaluation.features.access.exception;

public class InvalidInputException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -330870925286655017L;

	public InvalidInputException(String message) {
		super(message);
	}
}